use super::led::Led;
use alloc::vec::Vec;
use core::fmt::Display;
use smart_leds::RGB8;

pub enum Plane {
    XY,
    YZ,
    XZ,
}

const PCB_LED_DIST: f32 = 33.5;
pub const PCB_LED_OFFSET: f32 = 40.0;

#[derive(Default)]
pub struct Side {
    pub leds: [Led; 9],
}

impl Display for Side {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        for (i, led) in self.leds.iter().enumerate() {
            write!(f, "{} ", led)?;
            if (i + 1) % 3 == 0 {
                writeln!(f)?;
            }
        }
        Ok(())
    }
}

// LED order on each side is left to right from top to bottom
impl Side {
    pub fn new_on_plane(plane: Plane, plane_offset: f32, plane_flip: bool) -> Self {
        let mut x: f32 = 0.0;
        let mut y: f32 = 0.0;
        let mut z: f32 = 0.0;
        let mut matrix_x = [-PCB_LED_DIST, 0.0, PCB_LED_DIST];
        let matrix_y = [PCB_LED_DIST, 0.0, -PCB_LED_DIST];
        if plane_flip {
            matrix_x.reverse();
        }
        let mut leds: [Led; 9] = [Led::default(); 9];
        let mut pos = 0;
        for yy in matrix_y {
            for xx in matrix_x {
                let (zp, xp, yp) = match plane {
                    Plane::YZ => (&mut x, &mut y, &mut z),
                    Plane::XZ => (&mut y, &mut x, &mut z),
                    Plane::XY => (&mut z, &mut x, &mut y),
                };
                *xp = xx;
                *yp = yy;
                *zp = plane_offset;
                leds[pos].position.x = x;
                leds[pos].position.y = y;
                leds[pos].position.z = z;
                pos += 1;
            }
        }
        Self { leds }
    }

    pub fn top_left(&self) -> &Led {
        &self.leds[0]
    }

    pub fn top(&self) -> &Led {
        &self.leds[1]
    }

    pub fn top_right(&self) -> &Led {
        &self.leds[2]
    }

    pub fn middle_left(&self) -> &Led {
        &self.leds[3]
    }

    pub fn middle(&self) -> &Led {
        &self.leds[4]
    }

    pub fn middle_right(&self) -> &Led {
        &self.leds[5]
    }

    pub fn bottom_left(&self) -> &Led {
        &self.leds[6]
    }

    pub fn bottom(&self) -> &Led {
        &self.leds[7]
    }

    pub fn bottom_right(&self) -> &Led {
        &self.leds[8]
    }

    pub fn all_leds(&self) -> Vec<&Led> {
        self.leds.iter().collect()
    }

    pub fn all_leds_mut(&mut self) -> Vec<&mut Led> {
        self.leds.iter_mut().collect()
    }

    pub fn set_color(&mut self, color: RGB8) {
        for led in self.leds.iter_mut() {
            led.color = color;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn display_default() {
        let side = Side::default();
        println!("{}", side);
    }

    #[test]
    fn new_on_plane_xy() {
        let side = Side::new_on_plane(Plane::XY, 0.0, false);
        println!("{}", side);
    }

    #[test]
    fn new_on_plane_xy_flipped() {
        let side = Side::new_on_plane(Plane::XY, 0.0, true);
        println!("{}", side);
    }

    #[test]
    fn new_on_plane_yz() {
        let side = Side::new_on_plane(Plane::YZ, 0.0, false);
        println!("{}", side);
    }

    #[test]
    fn new_on_plane_yz_flipped() {
        let side = Side::new_on_plane(Plane::YZ, 0.0, true);
        println!("{}", side);
    }

    #[test]
    fn new_on_plane_xz() {
        let side = Side::new_on_plane(Plane::XZ, 0.0, false);
        println!("{}", side);
    }

    #[test]
    fn new_on_plane_xz_flipped() {
        let side = Side::new_on_plane(Plane::XZ, 0.0, true);
        println!("{}", side);
    }

    #[test]
    fn all_leds() {
        let side = Side::new_on_plane(Plane::XY, 0.0, false);
        for led in side.all_leds() {
            println!("{}", led);
        }
    }
}

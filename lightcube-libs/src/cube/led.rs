use core::fmt::Display;
use smart_leds::RGB8;

#[derive(Default, Clone, Copy)]
pub struct Position {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Display for Position {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(
            f,
            "(X:{:+05.1},Y:{:+05.01},Z:{:+05.01})",
            self.x, self.y, self.z
        )
    }
}

#[derive(Default, Clone, Copy)]
pub struct Led {
    pub position: Position,
    pub color: RGB8,
}

impl Display for Led {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "[{},{}]", self.position, self.color)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn display_default() {
        let led = Led::default();
        println!("{}", led);
    }

    #[test]
    fn display() {
        let led = Led {
            position: Position {
                x: 1.0,
                y: 2.0,
                z: 3.0,
            },
            color: RGB8 {
                r: 64,
                g: 127,
                b: 255,
            },
        };
        println!("{}", led);
    }
}

# LightCube

LightCube is an interactive glowing cube toy dedicated to my little baby boy, designed from the top to the bottom by me.

Features:

- RP2040 MCU running application written in [Rust](https://www.rust-lang.org/)
- LSM6DS3TR-C Accel & Gyro (see my [driver](https://gitlab.com/mtczekajlo/lsm6ds3tr-rs))
- WS2813-C Programmable LEDs
- 18650 battery charging circuit via USB-C port

## Videos

Click to open in YouTube

[![Animations test](https://img.youtube.com/vi/myPT6BNJGhA/0.jpg)](http://www.youtube.com/watch?v=myPT6BNJGhA)

[![Plexis](https://img.youtube.com/vi/oXC_EWW0-L4/0.jpg)](https://www.youtube.com/watch?v=oXC_EWW0-L4)

[![PCBs](https://img.youtube.com/vi/Edkw7cjrgVU/0.jpg)](https://www.youtube.com/watch?v=Edkw7cjrgVU)

## Credits

- **[Marcin Osiński](https://www.linkedin.com/in/marcin-r-osinski/)** - a big, big **thank you** for several PCB reviews, electronics-related support and simply being there for me from the beginning of this project at any time of day and night. Check out his projects [here](https://gitlab.com/osinski).
- [Rafał Kramek](https://www.linkedin.com/in/rafa%C5%82-kramek-38b7b68b/) - thank you for the live PCB review session and latter offline reviews.

## Related projects

- CI Docker image: <https://gitlab.com/mtczekajlo/lightcube-docker>
- PCBs: <https://gitlab.com/mtczekajlo/lightcube-hardware>
- 3D printed parts and assembly: <https://cad.onshape.com/documents/e5399d38b19fcfa20c95bb11>

## Prerequisites

Install rust toolchain for `thumbv6m-none-eabi` target.

```bash
rustup self update
rustup update stable nightly
rustup target add thumbv6m-none-eabi
```

Install handy cargo extensions.

```bash
cargo install flip-link elf2uf2-rs probe-run
```

Install `openocd` supporting `picoprobe`.

```bash
git submodule update --init
pushd openocd
./bootstrap
./configure --enable-picoprobe --disable-werror
make -j"$(nproc)"
popd
```

Install VSCode `Cortex-Debug` debugging extension.

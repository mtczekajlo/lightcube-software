#![no_std]
#![no_main]

extern crate alloc;
use alloc::vec::Vec;
use defmt::*;
use defmt_rtt as _;
use embedded_alloc::Heap;
use fugit::RateExtU32;
use hal::Timer;
use lightcube_libs::cube::Cube;
use lsm6ds3tr::{interface::SpiInterface, LsmSettings, LSM6DS3TR};
use panic_halt as _;
use rand::rngs::SmallRng;
use rand::SeedableRng;
use rp_pico::entry;
use rp_pico::hal;
use rp_pico::hal::pac;
use rp_pico::hal::prelude::*;
use smart_leds::{SmartLedsWrite, RGB8};
use ws2812_pio::Ws2812;

mod animations;

#[global_allocator]
static HEAP: Heap = Heap::empty();

#[entry]
fn main() -> ! {
    //* Setup
    {
        use core::mem::MaybeUninit;
        const HEAP_SIZE: usize = 1024;
        static mut HEAP_MEM: [MaybeUninit<u8>; HEAP_SIZE] = [MaybeUninit::uninit(); HEAP_SIZE];
        unsafe { HEAP.init(HEAP_MEM.as_ptr() as usize, HEAP_SIZE) }
    }
    let mut _small_rng = SmallRng::seed_from_u64(0xDEADBEEFDEADBEEF);
    let mut pac = pac::Peripherals::take().expect("Peripherals access failure!");
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);
    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .expect("Clocks initialization failure!");
    let core = pac::CorePeripherals::take().expect("Core peripherals access failure!");
    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().to_Hz());
    let sio = hal::Sio::new(pac.SIO);
    let pins = rp_pico::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );
    let timer = Timer::new(pac.TIMER, &mut pac.RESETS);
    let (mut pio, sm0, _, _, _) = pac.PIO0.split(&mut pac.RESETS);
    let mut ws = Ws2812::new(
        pins.gpio6.into_mode(),
        &mut pio,
        sm0,
        clocks.peripheral_clock.freq(),
        timer.count_down(),
    );
    let _spi_sclk = pins.gpio0.into_mode::<hal::gpio::FunctionSpi>();
    let spi_cs = pins.gpio1.into_push_pull_output();
    let _spi_miso = pins.gpio2.into_mode::<hal::gpio::FunctionSpi>();
    let _spi_mosi = pins.gpio3.into_mode::<hal::gpio::FunctionSpi>();
    let spi = hal::Spi::<_, _, 8>::new(pac.SPI0);
    let spi = spi.init(
        &mut pac.RESETS,
        clocks.peripheral_clock.freq(),
        8.MHz(),
        &embedded_hal::spi::MODE_0,
    );

    let spi_interface = SpiInterface::new(spi, spi_cs);
    let mut imu = LSM6DS3TR::new(spi_interface).with_settings(LsmSettings::basic());
    imu.init().expect("LSM initialization failure!");

    //* App
    let mut cube = Cube::new();
    let mut leds = cube.all_leds_mut();
    let mut small_rng = SmallRng::seed_from_u64(0xDEADBEEFDEADBEEF);

    info!("All initialized!");

    loop {
        for i in 0..leds.len() {
            animations::wheel(i as u8, &mut leds);
            animations::dot(i, &mut leds);
            let colors: Vec<RGB8> = leds.iter().map(|led| led.color).collect();
            ws.write(colors.iter().copied()).unwrap();
            delay.delay_ms(16);
        }
        if let Ok(xyz) = imu.read_accel() {
            animations::rotation_color(&xyz, &mut leds);
            animations::rotation_wheel(&xyz, &mut leds);
        }
        animations::random(&mut small_rng, &mut leds);
        animations::random_rgb(&mut small_rng, &mut leds);
        animations::random_rgbcmy(&mut small_rng, &mut leds);
        animations::random_rgb_brightness(&mut small_rng, &mut leds);
        animations::random_rgbcmy_brightness(&mut small_rng, &mut leds);
        animations::random_dot(&mut small_rng, &mut leds);
        animations::random_color_dot(&mut small_rng, &mut leds);

        let colors: Vec<RGB8> = leds.iter().map(|led| led.color).collect();
        ws.write(colors.iter().copied()).unwrap();
        delay.delay_ms(16);
    }
}
